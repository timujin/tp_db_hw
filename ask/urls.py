from django.conf.urls import patterns, url

from ask import views

urlpatterns = patterns('',
	url(r'^$', views.index, name='index'),
	url(r'^page/(?P<page_num>\d+)$', 'ask.views.page', name='page'),
	url(r'^question/(?P<question_id>\d+)$', 'ask.views.show_question', name='show_question'),
	url(r'^login/', 'django.contrib.auth.views.login', {'template_name':'login.html'}),
	url(r'^logout/', 'django.contrib.auth.views.logout', {'next_page':'/'}),
	url(r'^signup/', views.signup, name='signup'),
	url(r'^ask/', views.ask, name='ask'),
	url(r'^answer/(?P<question_id>\d+)$', 'ask.views.give_answer', name='give_answer'),
)
